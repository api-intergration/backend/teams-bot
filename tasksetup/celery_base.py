from celery import Celery

from .celeryconfig import constants


def make_celery(app):
	celery = Celery(
		'DNAC',
		broker=constants.CELERY_BROKER_URL,
		backend=constants.CELERY_RESULT_BACKEND)

	# celery.conf.update(app.config)

	class ContextTask(celery.Task):
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return self.run(*args, **kwargs)

	celery.Task = ContextTask
	return celery
