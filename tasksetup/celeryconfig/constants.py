import os

broker_heartbeat = 0

# List of modules to import when the Celery worker starts.
CELERY_IMPORTS = (
'blueprints.api.routes', 'backend.APIworker', 'backend.utils.functions',
'utils.commands','lib.config')
CELERY_INCLUDES = 'managment.routes.setupinfo'

## Using the database to store task state and results.
if 'REDIS_SERVER' in os.environ:
	CELERY_RESULT_BACKEND = 'redis://' + os.environ['REDIS_SERVER'] + ':6379/0'
	## Broker settings.
	CELERY_BROKER_URL = 'redis://' + os.environ['REDIS_SERVER'] + ':6379/0'


result_persistent = False

CELERY_ACCEPT_CONTENT = ['json', 'application/text','application/json']

result_serializer = 'json'

ACCEPT_CONTECT = ['json', 'application/text']

RESULT_SERIALIZER = 'json'
