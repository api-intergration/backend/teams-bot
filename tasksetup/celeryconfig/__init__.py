from .constants import CELERY_RESULT_BACKEND,CELERY_ACCEPT_CONTENT,\
	CELERY_BROKER_URL,CELERY_IMPORTS,CELERY_INCLUDES,\
	result_persistent,result_serializer,broker_heartbeat,\
	RESULT_SERIALIZER,ACCEPT_CONTECT
from .config import get_config