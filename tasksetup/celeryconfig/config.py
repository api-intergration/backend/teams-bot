import os
def get_config():
    class Config:
        DEBUG = True
        FLASK_DEBUG=True
        # Flask-Restplus settings

        """EXPLAIN_TEMPLATE_LOADING = True"""
        LOG_LEVEL = 'DEBUG'  # CRITICAL / ERROR / WARNING / INFO / DEBUG
        SECRET_KEY = 'CLUS-CISCO'
        WTF_CSRF_ENABLED = False
        # SQLAlchemy.
        """ username and pass in db_uri must match from .env file (POSTGRES_USER=) (POSTGRES_PASSWORD=) """

        SQLALCHEMY_DATABASE_URI = os.environ['LOCAL_SQL_CONNECTION']
        SQLALCHEMY_TRACK_MODIFICATIONS = False
        TEAMS_BOT_TOKEN = os.environ['TEAMS_BOT_TOKEN']
        TEAMS_BOT_EMAIL = os.environ['TEAMS_BOT_EMAIL']
        TEAMS_BOT_ROOM_ID = os.environ['TEAMS_BOT_ROOM_ID']
        MESSAGE = False
        CARDS = False

    return Config
