from celery import Celery
from flask import Flask
from .celeryconfig import config, constants
from APIBase.utils.extensions import db

def make_celery(app):
	"""
	
	Args:
		app:

	Returns:

	"""
	celery = Celery()
	# celery = Celery(
	#		result_backend= config.CELERY_RESULT_BACKEND,
	#		broker_url=config.CELERY_BROKER_URL,
	#
	#	)
	celery.conf.update(
		{
			'broker_url':        constants.CELERY_BROKER_URL,
			'result_backend':    constants.CELERY_RESULT_BACKEND,
			'task_routes':       ('tasksetup.utils_task_router.TaskRouter'),
			'result_serializer': constants.RESULT_SERIALIZER,
			 'accept_content':    constants.CELERY_ACCEPT_CONTENT
		}
	)

	class ContextTask(celery.Task):
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return self.run(*args, **kwargs)

	celery.Task = ContextTask
	return celery


def start_celery():
	appconfig = config.get_config()
	flask_app = Flask(__name__)
	flask_app.config.from_object(appconfig)
	db.init_app(flask_app)
	db.app = flask_app
	celery = make_celery(flask_app)
	# celery.conf.update({'task_routes': ('task_router.TaskRouter',)})

	return celery
