import APIBase.teamsutils
import botlib
import tasksetup
botlib.setup_webhook(botlib.get_public_url())
celery = tasksetup.start_celery()



@celery.task(bind=True,name='BOT:teams_new_device_message')
def worker_teams_new_device_message(self, device):
    attachment = APIBase.teamsutils.createCard.createNewDeviceCard(device)
    status = APIBase.teamsutils.newDeviceSendCard(attachment)
    botlib.apilogging.debug(f"Message Status: {str(status)}")
    if status:
        self.update_state(status='SUCCESS')
    else:
        self.update_state(status='FAILED')


