FROM registry.gitlab.com/api-intergration/server-settings/base:latest as TEAMS-BOT
ARG SYSTEM
ARG BOT
ARG SHARED
ARG DNACUTILS
ARG TEAMSUTILS
ARG TASKSETUP
ARG TOKEN
ENV TOKEN=$TOKEN
ENV BOT=${BOT}
ENV SHARED=$SHARED
ENV TEAMSUTILS=$TEAMSUTILS
ENV SYSTEM=$SYSTEM
ENV DNACUTILS=$DNACUTILS
ENV TASKSETUP=$TASKSETUP
#ADD https://gitlab.com/Josh.Lipton/dnac_intergrator/-/archive/master/dnac_intergrator-master.tar.gz /opt/app/master.tar.gz
#RUN tar -zvxf /opt/app/master.tar.gz -C /opt/app/ --strip-components 1

#RUN pip install git+https://__token__:$TOKEN@gitlab.com/api-intergration/utils/shared.git

WORKDIR /opt/app
RUN curl --header "PRIVATE-TOKEN: $TOKEN"  https://gitlab.com/api/v4/projects/$BOT/repository/archive.tar.gz -O
RUN tar -zxvf /opt/app/archive.tar.gz -C /opt/app --strip-components 1


#USER root
#RUN rm -R /opt/app/

WORKDIR /opt/app
#CMD dnacsync tasks start




