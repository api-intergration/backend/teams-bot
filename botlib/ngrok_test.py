import requests
import os

def get_public_url():
	try:
		response = requests.get(url=os.environ['NGROK_CLIENT_API_BASE_URL'] + "/tunnels",
								headers={
									'content-type': 'application/json'})
		response.raise_for_status()

	except requests.exceptions.RequestException:
		print("Could not connect to the ngrok client API; "
			  "assuming not running.")
		return os.environ['PUBLIC_URL']

	else:
		for tunnel in response.json()["tunnels"]:
			if tunnel.get("public_url", "").startswith("http://"):
				print("Found ngrok public HTTP URL:", tunnel["public_url"])
				return tunnel["public_url"]
