import logging
import os

apilogging = logging
if os.environ['RUN_STATE'] == 'DEV':

    apilogging.basicConfig(filename='../../../task-server.log',
						   level=apilogging.DEBUG,
						   format='%(asctime)s %(levelname)s %(message)s')
    apilogging.debug('Local Development apilogging Enabled')
else:
    apilogging.basicConfig(filename='/opt/logs/bot-server.log',level=apilogging.DEBUG,format='%(asctime)s %(levelname)s %(message)s')
    apilogging.debug('Local Production apilogging Enabled')
