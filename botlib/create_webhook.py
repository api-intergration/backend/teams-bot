import logging
import os
from urllib.parse import urljoin
from webexteamssdk import webexteamssdkException
from APIBase.utils.utils_teams import teams_api


def delete_teams_webhooks(teams, name):
    """Find a webhook by name."""
    try:
        for webhook in teams.webhooks.list():
            if webhook.name == name:
                logging.debug(
                    f"Deleting Webhook:  {webhook.name} {webhook.targetUrl}")

                teams.webhooks.delete(webhook.id)
    except webexteamssdkException as e:
        logging.error(f"Deleting webhooks failed error: {str(e)}")


def create_teams_webhook(teams, url):
    try:
        webhook = teams.webhooks.create(
            name=os.environ['TEAMS_WEBHOOK_NAME'],
            targetUrl=urljoin(url, os.environ['TEAMS_WEBHOOK_URL_SUFFIX']),
            resource=os.environ['TEAMS_WEBHOOK_RESOURCE'],
            event=os.environ['TEAMS_WEBHOOK_EVENT'],
        )
        logging.debug(webhook)
        logging.debug("Webhook successfully created.")
        return webhook
    except (webexteamssdkException, Exception) as e:
        logging.error(f"Creating webhooks failed error: {str(e)}")
        return False


def create_card_webhook(teams, url):
    try:
        webhook = teams.webhooks.create(
            name=os.environ['TEAMS_WEBHOOK_NAME'],
            targetUrl=urljoin(url, 'api/v1/bot/cardResponse'),
            resource='attachmentActions',
            event='created',
        )
        logging.debug(webhook)
        logging.debug("Webhook successfully created.")
        return webhook

    except webexteamssdkException as e:
        logging.error(f"Creating webhooks failed error: {str(e)}")
        return False


def setup_webhook(public_url):
    delete_teams_webhooks(teams_api, str(os.environ['TEAMS_WEBHOOK_NAME']))
    hook = create_teams_webhook(teams_api, public_url)
    print(hook)
    hook2 = create_card_webhook(teams_api, public_url)
    print(hook2)
